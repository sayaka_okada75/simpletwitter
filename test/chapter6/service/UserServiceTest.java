package chapter6.service;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.CompositeTable;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.User;
import chapter6.utils.CipherUtil;
import chapter6.utils.DBUtil;
import junit.framework.TestCase;

// 1. テストクラス名は任意のものに変更してください。
// 2. L.23~86は雛形として使用してください。
// 3．L.44のファイル名は各自作成したファイル名に書き換えてください。
public class UserServiceTest extends TestCase {

	private File file;

	@Before
	public void setUp() throws Exception {

		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("users");
			partialDataSet.addTable("messages");

			file = File.createTempFile("temp", ".xml");
			FlatXmlDataSet.write(partialDataSet,
					new FileOutputStream(file));

			//(3)テストデータを投入する
			IDataSet dataSetBranch = new FlatXmlDataSet(new File("user_test_data.xml"));
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetBranch);

			DBUtil.commit(conn);

//			IDataSet databaseDataSet = connection.createDataSet();
//			ITable actualTable = databaseDataSet.getTable("messages");
//
//			System.out.println("");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			IDataSet dataSet = new FlatXmlDataSet(file);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			//一時ファイルの削除
			if (file != null) {
				file.delete();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}

		}
	}

	// 以下に課題のテストメソッドを作成

	/**
	* insertメソッドのテスト
	*/
	@Test
	public void testInsertUser() throws Exception {

		//テスト対象となる、storeメソッドを実行
		//テストのインスタンスを生成

		User user1 = new User();
		user1.setAccount("山本");
		user1.setName("山本");
		user1.setEmail("yama@gmail.com");
		user1.setPassword("yama");
		user1.setDescription("yama");


		UserService insertTest = new UserService();
		insertTest.insert(user1);
		String dePassword = CipherUtil.base64Decode(user1.getPassword());
		user1.setPassword(dePassword);


		//テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			//Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			//メソッド実行した実際のテーブル
			IDataSet databaseDataSet = connection.createDataSet();
			ITable actualTable = databaseDataSet.getTable("users");

			// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
			//branch_test_data2.xmlのbranch_sale_outのデータをexpectedTableに定義する
			IDataSet expectedDataSet = new FlatXmlDataSet(new File("user_test_data2.xml"));
			ITable expectedTable = expectedDataSet.getTable("users");

			//期待されるITableと実際のITableの比較
			//日付を無視する設定
			Assertion.assertEquals(expectedTable,new CompositeTable(expectedTable.getTableMetaData(), actualTable));

		} finally {
			if (connection != null)
				connection.close();
		}

	}






}